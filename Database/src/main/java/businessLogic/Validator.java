package businessLogic;

import model.*;

public class Validator {
	
	private static final String CLIENT_NAME_PATTERN = "[a-zA-z]*[ -]?[a-zA-Z]*";
	private static final String PRODUCT_NAME_PATTERN = "[a-zA-z ]*";
	
	public static void validate(Product p) {
		if(!p.getName().matches(PRODUCT_NAME_PATTERN))
			throw new IllegalArgumentException("Name of product may only contain letters and whitespaces!");
		if(p.getStock() < 0) {
			throw new IllegalArgumentException("Stock cannot be negative!");
		}
		if(p.getPrice() <= 0) {
			throw new IllegalArgumentException("Price must be larger than 0!");
		}
	}
	
	public static void validate(Client c) {
		if(!c.getName().matches(CLIENT_NAME_PATTERN) || !c.getSurname().matches(CLIENT_NAME_PATTERN)) {
			throw new IllegalArgumentException("Name and surname of client many only contain letters and whitespaces!");
		}
	}
	
	public static void validate(ProductOrder o) {
		if(o.getProductAmount() <= 0) {
			throw new IllegalArgumentException("Amount must be larger than 0!");
		}
	}
}
