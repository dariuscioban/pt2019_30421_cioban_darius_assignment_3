package businessLogic;

import java.util.NoSuchElementException;
import java.util.ArrayList;
import model.*;
import dataAccess.ProductOrderDA;

public class ProductOrderBLL {
	
	public static ArrayList<Object> findAllAsObject() {
		ArrayList<Object> ret = new ArrayList<Object>();
		for(ProductOrder o : findAll()) {
			ret.add(o);
		}
		return ret;
	}
	
	public static ArrayList<ProductOrder> findAll() {
		ArrayList<ProductOrder> ret = ProductOrderDA.findAll();
		if(ret == null) {
			throw new NoSuchElementException("Table has no entries.");
		}
		return ret;
	}
	
	public static ProductOrder findById(int id) {
		ProductOrder o = ProductOrderDA.findById(id);
		if(o == null) {
			throw new NoSuchElementException("The order with id =" + id + " was not found!");
		}
		return o;
	}
	
	public static int insertOrder(Product p, Client c, int amount) {
		int ret;
		ProductOrder o = new ProductOrder(0, c.getId(), p.getId(), amount, p.getPrice() * amount);
		ProductBLL.decrementStock(p.getId(), amount);
		ret = insertOrder(o);
		return ret;
	}
	
	public static int insertOrder(ProductOrder o) {
		Validator.validate(o);
		return ProductOrderDA.insert(o);
	}
	
}
