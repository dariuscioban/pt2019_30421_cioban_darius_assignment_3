package businessLogic;

import java.util.NoSuchElementException;
import java.util.ArrayList;
import model.Product;
import dataAccess.ProductDA;

public class ProductBLL {
	
	public static ArrayList<Object> findAllAsObject() {
		ArrayList<Object> ret = new ArrayList<Object>();
		for(Product p : findAll()) {
			ret.add(p);
		}
		return ret;
	}
	
	public static ArrayList<Product> findAll() {
		ArrayList<Product> ret = ProductDA.findAll();
		if(ret == null) {
			throw new NoSuchElementException("Table has no entries.");
		}
		return ret;
	}
	public static Product findById(int id) {
		Product p = ProductDA.findById(id);
		if(p == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return p;
	}
	
	public static void deleteProduct(int id) {
		findById(id);
		ProductDA.delete(id);
	}
	
	public static int insertProduct(String name, int stock, int price) {
		Product p = new Product(0, name, stock, price);
		return insertProduct(p);
	}
	
	public static int insertProduct(Product p) {
		Validator.validate(p);
		return ProductDA.insert(p);
	}
	
	public static void decrementStock(int id, int amount) {
		Product p = findById(id);
		int newStock = p.getStock() - amount;
		if(newStock < 0) {
			throw new IllegalArgumentException("Amount cannot be larger than product's stock! Current stock is " + p.getStock() + ".");
		}
		p.setStock(newStock);
		ProductDA.update(p, id);
	}
	
	public static void updateProduct(int id, String name, int stock, int price) {
		Product p = new Product(0, name, stock, price);
		updateProduct(id, p);
	}
	
	public static void updateProduct(int id, Product p) {
		Validator.validate(p);
		findById(id);
		ProductDA.update(p, id);
	}
}
