package businessLogic;

import java.util.NoSuchElementException;
import java.util.ArrayList;
import model.Client;
import dataAccess.ClientDA;

public class ClientBLL {
	
	public static ArrayList<Object> findAllAsObject() {
		ArrayList<Object> ret = new ArrayList<Object>();
		for(Client c : findAll()) {
			ret.add(c);
		}
		return ret;
	}
	
	public static ArrayList<Client> findAll() {
		ArrayList<Client> ret = ClientDA.findAll();
		if(ret == null) {
			throw new NoSuchElementException("Table has no entries.");
		}
		return ret;
	}
	
	public static Client findById(int id) {
		Client c = ClientDA.findById(id);
		if(c == null) {
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		}
		return c;
	}
	
	public static void deleteClient(int id) {
		findById(id);
		ClientDA.delete(id);
	}
	
	public static int insertClient(String name, String surname) {
		Client c = new Client(0, name, surname);
		return insertClient(c);
	}
	
	public static int insertClient(Client c) {
		Validator.validate(c);
		return ClientDA.insert(c);
	}
	
	public static void updateClient(int id, String name, String surname) {
		Client c = new Client(0, name, surname);
		updateClient(id, c);
	}
	
	public static void updateClient(int id, Client c) {
		Validator.validate(c);
		findById(id);
		ClientDA.update(c, id);
	}
}
