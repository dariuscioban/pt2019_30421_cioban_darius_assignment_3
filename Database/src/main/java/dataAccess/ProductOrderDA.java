package dataAccess;

import java.sql.*;
import java.util.ArrayList;
import model.ProductOrder;

public class ProductOrderDA {
	
	private static final String insertStatementString = "INSERT INTO productorder (clientID,productID,productAmount,totalPrice)"
			+ " VALUES (?,?,?,?)";
	private static final String findStatementString = "SELECT * FROM productorder WHERE orderID = ?";
	private static final String deleteStatementString = "DELETE FROM productorder WHERE orderID = ?";
	private static final String updateStatementString = "UPDATE productorder SET clientID = ?, productID = ?, productAmount = ?"
			+ ", totalPrice = ? WHERE orderID = ?";
	private static final String selectAllStatementString = "SELECT * FROM productorder";
	
	public static ArrayList<ProductOrder> findAll() {
		ArrayList<ProductOrder> ret = new ArrayList<ProductOrder>();
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet result = null;
		
		try {
			statement = connection.prepareStatement(selectAllStatementString);
			result = statement.executeQuery();
			while(result.next()) {
				ProductOrder temp = new ProductOrder(result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4), result.getInt(5));
				ret.add(temp);
				
			}
		} catch (SQLException e) {
			
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return ret;
	}
	
	public static ProductOrder findById(int id) {
		ProductOrder ret = null;
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet result = null;
		
		try {
			statement = connection.prepareStatement(findStatementString);
			statement.setInt(1, id);
			result = statement.executeQuery();
			result.next();

			int clientID = result.getInt("clientID");
			int productID = result.getInt("ProductID");
			int productAmount = result.getInt("prouctAmount");
			int totalPrice = result.getInt("totalPrice");
			ret = new ProductOrder(id, clientID, productID, productAmount, totalPrice);
		} catch (SQLException e) {

		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
		return ret;
	}
	
	public static int insert(ProductOrder order) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		int insertedId = -1;
		
		try {
			statement = connection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, order.getClientID());
			statement.setInt(2, order.getProductID());
			statement.setInt(3, order.getProductAmount());
			statement.setInt(4, order.getTotalPrice());
			statement.executeUpdate();

			ResultSet result = statement.getGeneratedKeys();
			if (result.next()) {
				insertedId = result.getInt(1);
			}
		} catch (SQLException e) {

		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return insertedId;
	}
	
	public static void delete(int id) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement(deleteStatementString);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			
		} finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(statement);
		}
		
	}
	
	public static void update(ProductOrder order,int id) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement(updateStatementString);
			statement.setInt(1, order.getClientID());
			statement.setInt(2, order.getProductID());
			statement.setInt(3, order.getProductAmount());
			statement.setInt(3, order.getTotalPrice());
			statement.setInt(4, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			
		} finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(statement);
		}
	}
}
