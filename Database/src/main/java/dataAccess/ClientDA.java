package dataAccess;

import java.sql.*;
import model.Client;
import java.util.ArrayList;

public class ClientDA {
	
	private static final String insertStatementString = "INSERT INTO client (name,surname)"
			+ " VALUES (?,?)";
	private static final String findStatementString = "SELECT * FROM client WHERE id = ?";
	private static final String deleteStatementString = "DELETE FROM client WHERE id = ?";
	private static final String updateStatementString = "UPDATE client SET name = ?, surname = ? WHERE id = ?";
	private static final String selectAllStatementString = "SELECT * FROM client";
	
	public static ArrayList<Client> findAll() {
		ArrayList<Client> ret = new ArrayList<Client>();
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet result = null;
		
		try {
			statement = connection.prepareStatement(selectAllStatementString);
			result = statement.executeQuery();
			while(result.next()) {
				Client temp = new Client(result.getInt(1), result.getString(2), result.getString(3));
				ret.add(temp);
				
			}
		} catch (SQLException e) {
			
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return ret;
	}
	
	public static Client findById(int id) {
		Client ret = null;
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet result = null;
		
		try {
			statement = connection.prepareStatement(findStatementString);
			statement.setInt(1, id);
			result = statement.executeQuery();
			result.next();

			String name = result.getString("name");
			String surname = result.getString("surname");
			ret = new Client(id, name, surname);
		} catch (SQLException e) {

		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
		return ret;
	}
	
	public static int insert(Client client) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		int insertedId = -1;
		
		try {
			statement = connection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, client.getName());
			statement.setString(2, client.getSurname());
			statement.executeUpdate();

			ResultSet result = statement.getGeneratedKeys();
			if (result.next()) {
				insertedId = result.getInt(1);
			}
		} catch (SQLException e) {

		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return insertedId;
	}
	
	public static void delete(int id) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement(deleteStatementString);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {

		} finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(statement);
		}
		
	}
	
	public static void update(Client client,int id) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement(updateStatementString);
			statement.setString(1, client.getName());
			statement.setString(2, client.getSurname());
			statement.setInt(3, id);
			statement.executeUpdate();
		} catch (SQLException e) {

		} finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(statement);
		}
	}
}
