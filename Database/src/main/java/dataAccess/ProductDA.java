package dataAccess;

import java.sql.*;
import java.util.ArrayList;
import model.Product;

public class ProductDA {
	
	private static final String insertStatementString = "INSERT INTO product (name,stock,price)"
			+ " VALUES (?,?,?)";
	private static final String findStatementString = "SELECT * FROM product WHERE id = ?";
	private static final String deleteStatementString = "DELETE FROM product WHERE id = ?";
	private static final String updateStatementString = "UPDATE product SET name = ?, stock = ?, price = ? WHERE id = ?";
	private static final String selectAllStatementString = "SELECT * FROM product";
	
	public static ArrayList<Product> findAll() {
		ArrayList<Product> ret = new ArrayList<Product>();
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet result = null;
		
		try {
			statement = connection.prepareStatement(selectAllStatementString);
			result = statement.executeQuery();
			while(result.next()) {
				Product temp = new Product(result.getInt(1), result.getString(2), result.getInt(3), result.getInt(4));
				ret.add(temp);
				
			}
		} catch (SQLException e) {
			
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return ret;
	}
	
	public static Product findById(int id) {
		Product ret = null;
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet result = null;
		
		try {
			statement = connection.prepareStatement(findStatementString);
			statement.setInt(1, id);
			result = statement.executeQuery();
			result.next();

			String name = result.getString("name");
			int stock = result.getInt("stock");
			int price = result.getInt("price");
			ret = new Product(id, name, stock, price);
		} catch (SQLException e) {
			
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
		return ret;
	}
	
	public static int insert(Product product) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		int insertedId = -1;
		
		try {
			statement = connection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, product.getName());
			statement.setInt(2, product.getStock());
			statement.setInt(3, product.getPrice());
			statement.executeUpdate();

			ResultSet result = statement.getGeneratedKeys();
			if (result.next()) {
				insertedId = result.getInt(1);
			}
		} catch (SQLException e) {

		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return insertedId;
	}
	
	public static void delete(int id) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement(deleteStatementString);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {

		} finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(statement);
		}
		
	}
	
	public static void update(Product product,int id) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement(updateStatementString);
			statement.setString(1, product.getName());
			statement.setInt(2, product.getStock());
			statement.setInt(3, product.getPrice());
			statement.setInt(4, id);
			statement.executeUpdate();
		} catch (SQLException e) {

		} finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(statement);
		}
	}
}
