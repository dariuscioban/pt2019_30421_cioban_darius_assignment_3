package presentation;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import javax.swing.*;

import businessLogic.ClientBLL;
import businessLogic.ProductBLL;
import businessLogic.ProductOrderBLL;

public class UserInterface {
	
	private static final int INSERT_CLIENT = 0;
	private static final int INSERT_PRODUCT = 1;
	private static final int FIND_CLIENT = 2;
	private static final int FIND_PRODUCT = 3;
	private static final int DELETE_CLIENT = 4;
	private static final int DELETE_PRODUCT = 5;
	private static final int UPDATE_CLIENT = 6;
	private static final int UPDATE_PRODUCT = 7;
	private static final int PLACE_ORDER = 8;
	private static final int SHOW_CLIENT = 9;
	private static final int SHOW_PRODUCT = 10;
	private static final int SHOW_ORDER = 11;
	
	JPanel tablePanel = new JPanel();
	JPanel rightPanel = new JPanel();
	
	JTextField clientNameField = new JTextField(10);
	JTextField clientSurnameField = new JTextField(10);
	JTextField productNameField = new JTextField(10);
	JTextField productStockField = new JTextField(10);
	JTextField productPriceField = new JTextField(10);
	
	JTextField clientFindField = new JTextField(10);
	JTextField productFindField = new JTextField(10);
	
	JTextField clientUpdateIdField = new JTextField(10);
	JTextField clientUpdateNameField = new JTextField(10);
	JTextField clientUpdateSurnameField = new JTextField(10);
	
	JTextField productUpdateIdField = new JTextField(10);
	JTextField productUpdateNameField = new JTextField(10);
	JTextField productUpdateStockField = new JTextField(10);
	JTextField productUpdatePriceField = new JTextField(10);
	
	JTextField clientIdField = new JTextField(10);
	JTextField productIdField = new JTextField(10);
	JTextField amountField = new JTextField(10);
	
	private JFrame frame;
	
	public void initUI() {
		frame = new JFrame("Database management");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1400, 600);
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		JPanel showPanel = new JPanel();
		tablePanel.setLayout(new BorderLayout());
		JPanel insertPanel = new JPanel();
		insertPanel.setLayout(new BoxLayout(insertPanel, BoxLayout.Y_AXIS));
		JPanel findPanel = new JPanel();
		findPanel.setLayout(new BoxLayout(findPanel, BoxLayout.Y_AXIS));
		JPanel updatePanel = new JPanel();
		updatePanel.setLayout(new BoxLayout(updatePanel, BoxLayout.Y_AXIS));
		JPanel clientInsertPanel = new JPanel();
		JPanel productInsertPanel = new JPanel();
		JPanel clientFindPanel = new JPanel();
		JPanel productFindPanel = new JPanel();
		JPanel clientUpdatePanel = new JPanel();
		JPanel productUpdatePanel = new JPanel();
		JPanel orderPanel = new JPanel();
		
		JButton clientInsertButton = new JButton("Insert");
		clientInsertButton.addActionListener(new Clicker(INSERT_CLIENT));
		JButton productInsertButton = new JButton("Insert");
		productInsertButton.addActionListener(new Clicker(INSERT_PRODUCT));
		
		JButton clientFindButton = new JButton("Find");
		clientFindButton.addActionListener(new Clicker(FIND_CLIENT));
		JButton clientDeleteButton = new JButton("Delete");
		clientDeleteButton.addActionListener(new Clicker(DELETE_CLIENT));
		JButton productFindButton = new JButton("Find");
		productFindButton.addActionListener(new Clicker(FIND_PRODUCT));
		JButton productDeleteButton = new JButton("Delete");
		productDeleteButton.addActionListener(new Clicker(DELETE_PRODUCT));
		
		JButton clientUpdateButton = new JButton("Update");
		clientUpdateButton.addActionListener(new Clicker(UPDATE_CLIENT));
		JButton productUpdateButton = new JButton("Update");
		productUpdateButton.addActionListener(new Clicker(UPDATE_PRODUCT));
		
		JButton orderButton = new JButton("Place order");
		orderButton.addActionListener(new Clicker(PLACE_ORDER));
		
		JButton showClientButton = new JButton("Show clients");
		showClientButton.addActionListener(new Clicker(SHOW_CLIENT));
		JButton showProductButton = new JButton("Show products");
		showProductButton.addActionListener(new Clicker(SHOW_PRODUCT));
		JButton showOrderButton = new JButton("Show orders");
		showOrderButton.addActionListener(new Clicker(SHOW_ORDER));
		
		clientInsertPanel.add(new JLabel("CLIENT -> "));
		clientInsertPanel.add(new JLabel("Name: "));
		clientInsertPanel.add(clientNameField);
		clientInsertPanel.add(new JLabel("Surname: "));
		clientInsertPanel.add(clientSurnameField);
		clientInsertPanel.add(clientInsertButton);
		productInsertPanel.add(new JLabel("PRODUCT -> "));
		productInsertPanel.add(new JLabel("Name: "));
		productInsertPanel.add(productNameField);
		productInsertPanel.add(new JLabel("Stock: "));
		productInsertPanel.add(productStockField);
		productInsertPanel.add(new JLabel("Price: "));
		productInsertPanel.add(productPriceField);
		productInsertPanel.add(productInsertButton);
		
		clientFindPanel.add(new JLabel("CLIENT -> "));
		clientFindPanel.add(new JLabel("ID: "));
		clientFindPanel.add(clientFindField);
		clientFindPanel.add(clientFindButton);
		clientFindPanel.add(clientDeleteButton);
		productFindPanel.add(new JLabel("PRODUCT -> "));
		productFindPanel.add(new JLabel("ID: "));
		productFindPanel.add(productFindField);
		productFindPanel.add(productFindButton);
		productFindPanel.add(productDeleteButton);
		
		clientUpdatePanel.add(new JLabel("CLIENT -> "));
		clientUpdatePanel.add(new JLabel("ID: "));
		clientUpdatePanel.add(clientUpdateIdField);
		clientUpdatePanel.add(new JLabel("Name: "));
		clientUpdatePanel.add(clientUpdateNameField);
		clientUpdatePanel.add(new JLabel("Surname: "));
		clientUpdatePanel.add(clientUpdateSurnameField);
		clientUpdatePanel.add(clientUpdateButton);
		productUpdatePanel.add(new JLabel("PRODUCT -> "));
		productUpdatePanel.add(new JLabel("ID: "));
		productUpdatePanel.add(productUpdateIdField);
		productUpdatePanel.add(new JLabel("Name: "));
		productUpdatePanel.add(productUpdateNameField);
		productUpdatePanel.add(new JLabel("Stock: "));
		productUpdatePanel.add(productUpdateStockField);
		productUpdatePanel.add(new JLabel("Price: "));
		productUpdatePanel.add(productUpdatePriceField);
		productUpdatePanel.add(productUpdateButton);
		
		orderPanel.add(new JLabel("ORDER -> "));
		orderPanel.add(new JLabel("Client ID: "));
		orderPanel.add(clientIdField);
		orderPanel.add(new JLabel("Product ID: "));
		orderPanel.add(productIdField);
		orderPanel.add(new JLabel("Amount: "));
		orderPanel.add(amountField);
		orderPanel.add(orderButton);
		
		showPanel.add(showClientButton);
		showPanel.add(showProductButton);
		showPanel.add(showOrderButton);
		
		insertPanel.add(clientInsertPanel);
		insertPanel.add(productInsertPanel);
		findPanel.add(clientFindPanel);
		findPanel.add(productFindPanel);
		updatePanel.add(clientUpdatePanel);
		updatePanel.add(productUpdatePanel);
		leftPanel.add(insertPanel);
		leftPanel.add(new JSeparator());
		leftPanel.add(findPanel);
		leftPanel.add(new JSeparator());
		leftPanel.add(updatePanel);
		leftPanel.add(new JSeparator());
		leftPanel.add(orderPanel);
		rightPanel.add(showPanel);
		rightPanel.add(new JSeparator());
		rightPanel.add(tablePanel);
		mainPanel.add(leftPanel);
		mainPanel.add(new JSeparator());
		mainPanel.add(rightPanel);
		
		frame.setContentPane(mainPanel);
		frame.setVisible(true);
	}
	
	private class Clicker implements ActionListener {
		
		int mode;
		
		public Clicker(int mode) {
			this.mode = mode;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			switch (mode) {
				case INSERT_CLIENT:
					try {
						String name = clientNameField.getText();
						String surname = clientSurnameField.getText();
						ClientBLL.insertClient(name, surname);
					} catch(Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
					}
					break;
				case INSERT_PRODUCT:
					try {
						String name = productNameField.getText();
						int stock = Integer.parseInt(productStockField.getText());
						int price = Integer.parseInt(productPriceField.getText());
						ProductBLL.insertProduct(name, stock, price);
					} catch(Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
					}
					break;
				case FIND_CLIENT:
					try {
						int id = Integer.parseInt(clientFindField.getText());
						JOptionPane.showMessageDialog(frame, ClientBLL.findById(id).toString(), "Success!", JOptionPane.INFORMATION_MESSAGE);
					} catch(Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
					}
					break;
				case FIND_PRODUCT:
					try {
						int id = Integer.parseInt(productFindField.getText());
						JOptionPane.showMessageDialog(frame, ProductBLL.findById(id).toString(), "Success!", JOptionPane.INFORMATION_MESSAGE);
					} catch(Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
					}
					break;
				case DELETE_CLIENT:
					try {
						int id = Integer.parseInt(clientFindField.getText());
						ClientBLL.deleteClient(id);
					} catch(Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
					}
					break;
				case DELETE_PRODUCT:
					try {
						int id = Integer.parseInt(productFindField.getText());
						ProductBLL.deleteProduct(id);
					} catch(Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
					}
					break;
				case UPDATE_CLIENT:
					try {
						int id = Integer.parseInt(clientUpdateIdField.getText());
						String name = clientUpdateNameField.getText();
						String surname = clientUpdateSurnameField.getText();
						ClientBLL.updateClient(id, name, surname);
					} catch(Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
					}
					break;
				case UPDATE_PRODUCT:
					try {
						int id = Integer.parseInt(productUpdateIdField.getText());
						String name = productUpdateNameField.getText();
						int stock = Integer.parseInt(productUpdateStockField.getText());
						int price = Integer.parseInt(productUpdatePriceField.getText());
						ProductBLL.updateProduct(id, name, stock, price);
					} catch(Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
					}
					break;
				case PLACE_ORDER:
					try {
						int clientId = Integer.parseInt(clientIdField.getText());
						int productId = Integer.parseInt(productIdField.getText());
						int amount = Integer.parseInt(amountField.getText());
						int orderId = ProductOrderBLL.insertOrder(ProductBLL.findById(productId), ClientBLL.findById(clientId), amount);
						createBill(orderId, clientId, productId, amount);
					} catch(Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
					}
					break;
				case SHOW_CLIENT:
					try {
						tablePanel.removeAll();
						JTable table = createTable(ClientBLL.findAllAsObject());
						tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
						tablePanel.add(table, BorderLayout.CENTER);
						frame.revalidate();
					} catch(Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
					}
					break;
				case SHOW_PRODUCT:
					try {
						tablePanel.removeAll();
						JTable table = createTable(ProductBLL.findAllAsObject());
						tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
						tablePanel.add(table, BorderLayout.CENTER);
						frame.revalidate();
					} catch(Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
					}
					break;
				case SHOW_ORDER:
					try {
						tablePanel.removeAll();
						JTable table = createTable(ProductOrderBLL.findAllAsObject());
						tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
						tablePanel.add(table, BorderLayout.CENTER);
						frame.revalidate();
					} catch(Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
					}
					break;
			}
			
		}
		
	}
	
	private JTable createTable(ArrayList<Object> list) {
		JTable ret;
		int collumns = list.get(0).getClass().getDeclaredFields().length; 
		Object[][] entries = new Object[list.size()][collumns];
		String[] collumnNames = new String[collumns];
		for(int i = 0; i < collumns; i++) {
			collumnNames[i] = list.get(0).getClass().getDeclaredFields()[i].getName();
		}
		for(int i = 0; i < list.size(); i++) {
			Method[] methods = list.get(i).getClass().getDeclaredMethods();
			for(Method m : methods) {
				if(m.getName().contains("get")) {
					try {
						for(int j = 0; j < collumns; j++) {
							String substr = collumnNames[j].substring(0, 1).toUpperCase() + collumnNames[j].substring(1);
							if( m.getName().endsWith(substr))
								entries[i][j] = m.invoke(list.get(i));
						}
					} catch (Exception e) {
						
					}
				}
			}
		}
		ret = new JTable(entries, collumnNames);
		return ret;
	}
	
	private void createBill(int orderId, int clientId, int productId, int amount) {
		String filename = "order-no-" + Integer.toString(orderId) + ".txt";
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(filename, "UTF-8");
			writer.println(ClientBLL.findById(clientId).getName() + " " + ClientBLL.findById(clientId).getSurname()
					+ " has purchased the product " + ProductBLL.findById(productId).getName() + ".");
			writer.println("Quantity : " + amount + ".");
			writer.println("Price per unit : " + ProductBLL.findById(productId).getPrice() + ".");
			int totalPrice = ProductBLL.findById(productId).getPrice() * amount;
			writer.println("Total price : " + totalPrice + ".");
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			writer.close();
		}
	}
	
	public static void main(String[] args) {
		UserInterface ui = new UserInterface();
		ui.initUI();
	}
	
}