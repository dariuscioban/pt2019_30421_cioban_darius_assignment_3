package model;

public class ProductOrder {
	private int orderID;
	private int clientID;
	private int productID;
	private int productAmount;
	private int totalPrice;
	
	public ProductOrder(int orderID, int clientID, int productID, int productAmount, int totalPrice) {
		this.orderID = orderID;
		this.clientID = clientID;
		this.productID = productID;
		this.productAmount = productAmount;
		this.totalPrice = totalPrice;
	}

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public int getProductAmount() {
		return productAmount;
	}

	public void setProductAmount(int productAmount) {
		this.productAmount = productAmount;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	public String toString() {
		String toString = "ProductOrder [orderID=" + orderID + ", clientID=" + clientID + ", productID=" + productID +
				", productAmount=" + productAmount + ", totalPrice=" + totalPrice + "]";
		return toString;
	}
}
